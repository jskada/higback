package no.higback.bruker1.higback;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

//List view used was inspired by http://androidexample.com/Create_A_Simple_Listview_-_Android_Example/index.php?view=article_discription&aid=65&aaid=90
public class FrontPage extends Activity {


    //Thomas bytter til intent?
    public static String currentClassId = "";


    private String siteurl;

    private static final String jsonclassid = "classid";
    private static final String jsonclasscode = "classcode";
    private final List<String> classlist = new ArrayList<>();

    private ArrayAdapter<String> ourAdapter;
    private final ArrayList<String> values = new ArrayList<>();
    private final ArrayList<String[]> data = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        String error = getString(R.string.error);
        siteurl = getString(R.string.backendurl);
        String doesnotexist = getString(R.string.classnotexist);
        String about = getString(R.string.abouttext);
        String info = getString(R.string.information);

        new JSONParse().execute();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frontpage);


        //Temporary storage for our list items
        //From http://developer.android.com/guide/topics/data/data-storage.html
        //http://stackoverflow.com/questions/6030744/android-reading-from-file-openfileinput
        final String FILENAME = "temp_higback";

        //FileInputStream inputStreamReader = openFileInput("temp_higback.txt");


        //Alert dialog from http://stackoverflow.com/questions/26097513/android-simple-alert-dialog
        //To be shown if user leaves input field empty but still clicks the add class button
        final AlertDialog alertDialog = new AlertDialog.Builder(FrontPage.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Class input required");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        // Alert message for when the user inputs a class that does not exist
        final AlertDialog classDoesNotExist = new AlertDialog.Builder(FrontPage.this).create();
        classDoesNotExist.setTitle(error);
        classDoesNotExist.setMessage(doesnotexist);
        classDoesNotExist.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        //Another dialog to be shown as the info button in the right corner
        //Describes the app
        Button infoButton = (Button) findViewById(R.id.infoButton);
        final AlertDialog appInfoDialog = new AlertDialog.Builder(FrontPage.this).create();
        appInfoDialog.setTitle(info);
        appInfoDialog.setMessage(about);


        //Creates a button within the dialogs
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Yes Sir!",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        appInfoDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Cool!",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        //Getting variables
        //Whatever class the user decided to add
        final EditText userInput = (EditText) findViewById(R.id.userInput);
        Button buttonAdd = (Button) findViewById(R.id.create_buttonId);

        //Header for classes with an underline
        TextView yourClasses = (TextView) findViewById(R.id.your_classes);
        //following code from http://stackoverflow.com/questions/10019001/how-do-you-underline-a-text-in-android-xml
        yourClasses.setText(Html.fromHtml(getString(R.string.your_classes)));

        // Get ListView object from xml
        final ListView myList = (ListView) findViewById(R.id.front_page_list_view);

        // Define a new Adapter
        ourAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, values);

        // Assign adapter to ListView
        myList.setAdapter(ourAdapter);

        //If user clicks on the info button, show the info dialog
        infoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                appInfoDialog.show();
            }
        });

        //This adds strings to our values from user input
        //Inspired by http://stackoverflow.com/questions/15523966/add-item-in-array-list-of-android
        //http://stackoverflow.com/questions/10899335/adding-user-input-from-edit-text-into-list-view
        buttonAdd.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                //Collects input and converts it to a string
                String input = userInput.getText().toString().toUpperCase();

                //Save to temporary storage
                /* FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);

                fos.write(input.getBytes());
                fos.close(); */


                //Put the input into our list as long as there actually is an input
                if (input.length() > 0) {

                    if (classlist.contains(input)) {
                        //Add our new input
                        values.add(input);
                        //Notify adapter it has changed
                        ourAdapter.notifyDataSetChanged();

                        //Save input temporary to phone
                        save(FILENAME, input, getApplicationContext());

                        //Clear current text edits for new edits to be made
                        userInput.setText("");
                        //Prints it to the console/logcat
                        Log.e("n", "Added " + input + " to list view.");
                    }

                    if (!classlist.contains(input)) {
                        classDoesNotExist.show();
                    }

                } else {
                    //Show our alert dialog that an input is required
                    alertDialog.show();
                }

            }
        });


        // ListView Item Click Listener
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                //New intent for sending the user to main menu
                Intent changePage = new Intent(getApplicationContext(), MainActivity.class);

                // ListView Clicked item value
                String itemValue = (String) myList.getItemAtPosition(position);

                for (int i = 0; i < data.size(); i++) {
                    String[] pair = data.get(i);
                    if (pair[0].equals(itemValue)) {
                        //Thomas bytter til intent?
                        currentClassId = pair[1];
                        break;
                    }
                }

                //Sending user to main menu after clicking his/her respective class
                startActivity(changePage);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "You clicked: \n " + itemValue, Toast.LENGTH_LONG)
                        .show();

            }

        });

    }

    private class JSONParse extends AsyncTask<String, String, JSONArray> {

        @Override
        protected JSONArray doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            // Getting JSON from URL
            String postclasses = "getclasses";
            return jParser.getJSONFromUrl(siteurl, postclasses);
        }

        @Override
        protected void onPostExecute(JSONArray json) {
            try {
                // Add JSON to classlist
                for (int i = 0; i < json.length(); i++) {
                    String iteration = json.getJSONObject(i).getString(jsonclasscode);
                    String classid = json.getJSONObject(i).getString(jsonclassid);
                    classlist.add(iteration);
                    String[] pair = {iteration, classid};
                    data.add(pair);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private static void save(String filename, String theString,
                             Context ctx) {
        FileOutputStream fos;
        try {
            fos = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(theString);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}



