package no.higback.bruker1.higback;
/**
 * Created by Bruker1 on 08.12.2014.
 * This is the file used for quizzes
 */

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class quiz extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz);

        //Declare variables for the answer buttons
        Button answerA = (Button) findViewById(R.id.button_a);
        Button answerB = (Button) findViewById(R.id.button_b);
        Button answerC = (Button) findViewById(R.id.button_c);
        Button answerD = (Button) findViewById(R.id.button_d);

        //Create listeneres for answers
        answerA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Checking answer...", Toast.LENGTH_LONG).show();
            }
        });

        answerB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Checking answer...", Toast.LENGTH_LONG).show();
            }
        });

        answerC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Checking answer...", Toast.LENGTH_LONG).show();
            }
        });

        answerD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Checking answer...", Toast.LENGTH_LONG).show();
            }
        });

   /*@Override
   public void onClick(View view) {
     finish();
   }*/
    }
}