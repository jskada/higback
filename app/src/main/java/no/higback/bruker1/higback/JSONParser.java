package no.higback.bruker1.higback;

/* Based upon: http://www.learn2crack.com/2013/10/android-asynctask-json-parsing-example.html */

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

@SuppressWarnings("SameParameterValue")
class JSONParser {
    private static InputStream is = null;
    private static JSONArray jArr = null;
    private static String json = "";

    // constructor
    public JSONParser() {
    }

    public JSONArray getJSONFromUrl(String url, String postparam) {
        String[] postparams = {postparam};
        String[] values = {"1"};
        return getJSONFromUrl(url, postparams, values);
    }

    public JSONArray getJSONFromUrl(String url, String postparam, String value) {
        String[] postparams = {postparam};
        String[] values = {value};
        return getJSONFromUrl(url, postparams, values);
    }

    public JSONArray getJSONFromUrl(String url, String[] postparams, String[] values) {

        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            // Based upon: http://www.androidsnippets.com/executing-a-http-post-request-with-httpclient */
            // Add your data

            if (postparams.length != 0) {
                List<NameValuePair> nameValuePairs = new ArrayList<>(1);
                for (int i = 0; i < postparams.length; i++) {
                    nameValuePairs.add(new BasicNameValuePair(postparams[i], values[i]));
                }
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            }


            HttpResponse httpResponse = httpclient.execute(httppost);
            HttpEntity httpEntity = httpResponse.getEntity();

            is = httpEntity.getContent();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            json = sb.toString();

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        // try parse the string to a JSON object
        try {
            jArr = new JSONArray(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        // return JSON Array
        return jArr;
    }
}
