package no.higback.bruker1.higback;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;


public class FragmentB extends Fragment {

    //temporary data until app is in use
    private final int[] moods = {20, 4, 6};
    //defines moodcounts for later use several places in the code
    private TextView smilecount;
    private TextView mehcount;
    private TextView snorecount;

    public FragmentB() {

    }

    //method returning view of specific Fragment
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        //manipulate view
        final FrameLayout mFrameLayout = (FrameLayout) inflater.inflate(R.layout.fragment_b,
                container, false);
        //get moodcount textviews
        smilecount = (TextView) mFrameLayout.findViewById(R.id.smilecount);
        mehcount = (TextView) mFrameLayout.findViewById(R.id.mehcount);
        snorecount = (TextView) mFrameLayout.findViewById(R.id.snorecount);

        // this can be any View,it doesn't have to be a button
        //looking for a button with id = buttonmeh in inflated view
        Button mButton = (Button) mFrameLayout.findViewById(R.id.buttonsmile);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.ic_launcher_gsmile);
                mFrameLayout.findViewById(R.id.buttonmeh).setBackgroundResource(R.drawable.ic_launcher_meh);
                mFrameLayout.findViewById(R.id.buttonsnore).setBackgroundResource(R.drawable.ic_launcher_snore);
                setmoods(0);
            }
        });
        //looking for a button with id = buttonmeh in inflated view
        Button Button = (Button) mFrameLayout.findViewById(R.id.buttonmeh);
        Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.ic_launcher_gmeh);
                mFrameLayout.findViewById(R.id.buttonsmile).setBackgroundResource(R.drawable.ic_launcher_smile);
                mFrameLayout.findViewById(R.id.buttonsnore).setBackgroundResource(R.drawable.ic_launcher_snore);
                setmoods(1);
            }
        });
        //looking for a button with id = buttonsnore in inflated view
        Button kButton = (Button) mFrameLayout.findViewById(R.id.buttonsnore);
        kButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.ic_launcher_gsnore);
                mFrameLayout.findViewById(R.id.buttonsmile).setBackgroundResource(R.drawable.ic_launcher_smile);
                mFrameLayout.findViewById(R.id.buttonmeh).setBackgroundResource(R.drawable.ic_launcher_meh);
                setmoods(2);
            }
        });

        setmoods(-1);

        //return your layout to be shown after manipulation
        return mFrameLayout;
    }

    //sets and/or updates the chosen mood
    void setmoods(int mood) {
        //

        //gets temporary value
        int smiles = moods[0];
        int mehs = moods[1];
        int snores = moods[2];

        //puts temporary value to moodcounters
        smilecount.setText(String.valueOf(smiles));
        mehcount.setText(String.valueOf(mehs));
        snorecount.setText(String.valueOf(snores));

        //   if a button is pressed the value something
        if (mood != -1) {
            if (mood == 0) {
                smilecount.setText(String.valueOf(smiles + 1));
            } else if (mood == 1) {
                mehcount.setText(String.valueOf(mehs + 1));
            } else if (mood == 2) {
                snorecount.setText(String.valueOf(snores + 1));
            }
        }
    }
}
