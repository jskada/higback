package no.higback.bruker1.higback;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;


public class FragmentA extends Fragment {

    private ArrayAdapter<String> ourAdapter;

    public FragmentA() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inputTestView = inflater.inflate(R.layout.fragment_a, container, false);

        final Button commentButton = (Button) inputTestView.findViewById(R.id.commentButton);

        new JSONParse().execute();

        final EditText userInput = (EditText) inputTestView.findViewById(R.id.commentText);

        final ListView commentList = (ListView) inputTestView.findViewById(R.id.listComments);


        // Define a new Adapter
        ourAdapter = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1/*, values*/);

        ourAdapter.clear();

        // Assign adapter to ListView
        commentList.setAdapter(ourAdapter);


        commentButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //Collects input and converts it to a string
                String input = userInput.getText().toString();

                //Put the input into our list as long as there actually is an input
                if (input.length() > 0) {

                    //Notify adapter it has changed
                    ourAdapter.notifyDataSetChanged();


                    String comment = userInput.getText().toString();

                    //Clear current text edits for new edits to be made
                    userInput.setText("");
                    //Prints it to the console/logcat
                    Log.e("n", "Added " + input + " to list view.");

                    new PostStuff().execute(comment);

                    ourAdapter.clear();

                    new JSONParse().execute();

                    //Show our alert dialog that an input is required
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Input required", Toast.LENGTH_LONG).show();
                }

            }
        });

        // ListView Item Click Listener
        commentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item value
                String itemValue = (String) commentList.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getActivity().getApplicationContext(),
                        "You clicked comment: \n " + itemValue, Toast.LENGTH_LONG)
                        .show();

            }

        });


        return inputTestView;
    }

    private class JSONParse extends AsyncTask<String, String, JSONArray> {

        @Override
        protected JSONArray doInBackground(String... args) {


            JSONParser jParser = new JSONParser();
            // Getting JSON from URL
            JSONArray json;
            try {
                json = jParser.getJSONFromUrl(getString(R.string.commentsurl), "classid", FrontPage.currentClassId);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            //JSONArray json = jParser.getJSONFromUrl(getString(R.string.commentsurl), "classid", FrontPage.currentClassId);
            return json;
        }

        @Override
        protected void onPostExecute(JSONArray json) {
            try {
                // Add JSON to classlist
                if (json != null) {
                    for (int i = 0; i < json.length(); i++) {
                        String iteration = json.getJSONObject(i).getString("text");
                        ourAdapter.add(iteration);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private class PostStuff extends AsyncTask<String, String, JSONArray> {

        @Override
        protected JSONArray doInBackground(String... args) {

            String comment = args[0];

            String[] params = {"classid", "header", "text"};

            String[] values = {FrontPage.currentClassId, "header", comment};

            JSONParser jParser = new JSONParser();
            // Getting JSON from URL
            JSONArray json;
            try {
                json = jParser.getJSONFromUrl(getString(R.string.insertcommentsurl), params, values);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            return json;
        }

        @Override
        protected void onPostExecute(JSONArray json) {

        }

    }
}
