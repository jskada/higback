package no.higback.bruker1.higback;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.app.ActionBar.TabListener;
import android.support.v4.view.ViewPager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;

@SuppressWarnings("deprecation")
public class MainActivity extends FragmentActivity implements TabListener {

    private ViewPager viewPager;
    private ActionBar actionBar;

    //private String siteurl = getString(R.string.backendurl);


    @Override
    protected void onCreate(Bundle arg0) {

        super.onCreate(arg0);
        setContentView(R.layout.activity_main);

        //viewPager soley changes the fragments based on user swiping
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));
        //tells if the user is changing the page or not
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageSelected(int arg0) {
                // actionbar changes to what the user selects
                //noinspection deprecation
                actionBar.setSelectedNavigationItem(arg0);

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
        actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        //noinspection deprecation
        ActionBar.Tab
                tab1 = actionBar.newTab().setIcon(R.drawable.ic_launcher_comm);
        tab1.setTabListener(this);

        //noinspection deprecation
        ActionBar.Tab
                tab2 = actionBar.newTab().setIcon(R.drawable.ic_launcher_mood);
        tab2.setTabListener(this);

        //noinspection deprecation
        ActionBar.Tab
                tab3 = actionBar.newTab().setIcon(R.drawable.ic_launcher_quiz);
        tab3.setTabListener(this);

        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
        actionBar.addTab(tab3);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // viewPager changes to what tab was selected by user
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }
}

class MyAdapter extends FragmentPagerAdapter {

    public MyAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int arg0) {
        Fragment fragment = null;
        if (arg0 == 0) {
            fragment = new FragmentA();
        }
        if (arg0 == 1) {
            fragment = new FragmentB();
        }
        if (arg0 == 2) {
            fragment = new FragmentC();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

}