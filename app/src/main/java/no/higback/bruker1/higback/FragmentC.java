package no.higback.bruker1.higback;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


public class FragmentC extends Fragment {

    public FragmentC() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inputTestView = inflater.inflate(R.layout.fragment_c, container, false);

        Button uiButton = (Button) inputTestView.findViewById(R.id.uiButton);
        uiButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity().getApplicationContext(), "test", Toast.LENGTH_LONG).show();
                Intent i = new Intent(getActivity().getApplicationContext(), quiz.class);
                startActivity(i);
            }
        });

        // Inflate the layout for this fragment
        return inputTestView;
    }

}
