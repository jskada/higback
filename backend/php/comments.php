<?php

	// This file acts like a bridge between the server and the app. The file receives POST messages and returns JSON.
	header ('Content-type: application/json');
	
	ini_set('display_errors', 'On');
	error_reporting(E_ALL);
	
	require_once 'config.php';
	
	$sqljson = new sqljson();
	

	$comments = $sqljson -> getComments();
	echo json_encode($comments);
	
?>