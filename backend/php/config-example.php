<?php

	// This file contains settings necessary for running the app
	// This is the example file, fill in the proper settings and rename it to "config.php"
	
	// Debug settings WARNING: Might disclose sensitive information
	$config['debug'] = false; // enables debug features
	
	// Directory settings
	$config['dir'] = __DIR__;
	
	// SQL settings
	$config['sqlserver'] = 'localhost'; // Server for DB
	$config['sqldatabase'] = 'ssadmin'; // Which DB to connect to
	$config['sqluser'] = 'root'; // DB user
	$config['sqlpassword'] = ''; // User password

	// Loads up the database
	require_once 'db.php';
	
	// Load database class
	require_once 'logic/database.class.php';
	
	// Load user class
	require_once 'logic/sqljson.class.php';

?>