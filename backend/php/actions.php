<?php

	// This file acts like a bridge between the server and the app. The file receives POST messages and returns JSON.
	header ('Content-type: application/json');
	
	require_once 'config.php';
	
	$sqljson = new sqljson();
	
	if (isset($_POST['insertcomment'])) {
		$insertcomment = $sqljson -> insertComment();
		echo json_encode($comment);
	}
	
	
	if (isset($_POST['getclasses'])) {
		$classes = $sqljson -> getClass();
		echo json_encode($classes);
	}

	if (isset($_POST['getcomments'])) {
		$comments = $sqljson -> getComments();
		echo json_encode($comments);
	}
?>