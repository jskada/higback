-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 10, 2014 at 05:24 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.3-7+squeeze19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `higback`
--
CREATE DATABASE `higback` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `higback`;

-- --------------------------------------------------------

--
-- Table structure for table `Mood`
--

CREATE TABLE IF NOT EXISTS `Mood` (
  `moodid` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) NOT NULL,
  `mooda` varchar(32) COLLATE utf8_bin NOT NULL,
  `moodb` varchar(32) COLLATE utf8_bin NOT NULL,
  `moodc` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`moodid`),
  UNIQUE KEY `classid` (`classid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `Mood`
--


-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `classid` int(11) NOT NULL AUTO_INCREMENT,
  `classname` varchar(32) COLLATE utf8_bin NOT NULL,
  `classcode` varchar(32) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`classid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`classid`, `classname`, `classcode`) VALUES
(1, 'testtest', 'TEST1337'),
(2, 'Mobile Development Theory', 'IMT3662'),
(3, 'www-teknologies', 'IMT2291'),
(4, 'Mobile Development Project', 'IMT3672'),
(5, 'Webproject3', 'IMT3891');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `commentid` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) NOT NULL,
  `header` varchar(50) COLLATE utf8_bin NOT NULL,
  `text` varchar(255) COLLATE utf8_bin NOT NULL,
  `vote` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`commentid`),
  KEY `classid` (`classid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=38 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`commentid`, `classid`, `header`, `text`, `vote`) VALUES
(1, 2, 'Interesting ', 'I think this lecture was very interesting and I learn alot. ', 9),
(2, 5, 'We like Gerardo', 'Gerardo learnd us many new things today', 20),
(3, 5, 'Ahmet is nice', 'Ahmet gae us many good advice to our report', 10),
(4, 5, 'early morning', 'Today I was really tired and I had trubles  percieving what was teached to day. hope Ahmet post the slide on fronter.', 50),
(5, 5, 'Smart', 'The way gerardo used bootstrap to create a modal was very smart.', 30),
(9, 5, 'Repetition wanted', 'I think it was hard grasping all the terms and do not feel ready for taking the exam. Would be helpfull whit a repetition lecture', 30),
(18, 2, 'Liked the lecture today but.', 'Todays lecture was good but I would have liked if the part about layout was used more time on. ', 5),
(19, 2, 'Mariusz is a good teacher', 'Todays lecture was inspiring.  ', 40),
(20, 2, 'Need more classes like this one', 'I really like going to Mariusz''s classes. ', 35),
(21, 2, 'Funny', 'The joke about iphone was really funny.', 20),
(22, 1, 'test', 'test test test test', 34);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `questionid` int(11) NOT NULL AUTO_INCREMENT,
  `quizid` int(11) NOT NULL,
  `question` varchar(255) COLLATE utf8_bin NOT NULL,
  `answera` varchar(255) COLLATE utf8_bin NOT NULL,
  `answerb` varchar(255) COLLATE utf8_bin NOT NULL,
  `answerc` varchar(255) COLLATE utf8_bin NOT NULL,
  `answerd` varchar(255) COLLATE utf8_bin NOT NULL,
  `correctanswer` int(1) NOT NULL,
  PRIMARY KEY (`questionid`),
  KEY `quizid` (`quizid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`questionid`, `quizid`, `question`, `answera`, `answerb`, `answerc`, `answerd`, `correctanswer`) VALUES
(1, 1, 'How is User Interface often Pronounced?', 'USI', 'USIN', 'UI', 'USIE', 3),
(2, 1, 'What stands  GUI for?', 'Graphical User Interface', 'Graphic Under Interface', 'Graphics Under Interface', 'Graphical Under Interface', 1);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `quizid` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(11) NOT NULL,
  `quizname` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`quizid`),
  KEY `classid` (`classid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`quizid`, `classid`, `quizname`) VALUES
(1, 2, 'User Interface'),
(2, 2, 'Programming Android');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Mood`
--
ALTER TABLE `Mood`
  ADD CONSTRAINT `mood_FK` FOREIGN KEY (`classid`) REFERENCES `class` (`classid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_FK` FOREIGN KEY (`classid`) REFERENCES `class` (`classid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_FK` FOREIGN KEY (`quizid`) REFERENCES `quiz` (`quizid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `quiz`
--
ALTER TABLE `quiz`
  ADD CONSTRAINT `quiz_FK` FOREIGN KEY (`classid`) REFERENCES `class` (`classid`) ON DELETE NO ACTION ON UPDATE NO ACTION;
