<?php
	// This file opens a connection to the SQL database

	try {
		$db = new PDO('mysql:host='.$config['sqlserver'].';dbname='.$config['sqldatabase'].'', $config['sqluser'], $config['sqlpassword']);
		} 
	catch (PDOException $e) {
			print "Error! Kan ikke koble til server: " . $e->getMessage() . "<br/>";
			die();
		};
?>