<?php

	// This file acts like a bridge between the server and the app. The file receives POST messages and returns JSON.
	header ('Content-type: application/json');
	
	require_once 'config.php';
	
	$sqljson = new sqljson();
	
	$insertcomment = $sqljson -> insertComment();
	echo json_encode($comment);
	
	$file = "log.txt"; 
	$handle = fopen($file, "w");
	
	fwrite($handle, date("H:i:s") . "\n\n". var_export($_POST, true));
	fclose($handle);

?>