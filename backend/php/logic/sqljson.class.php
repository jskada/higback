<?php
	// This class is meant to handle most user activities

	class sqljson {
	
		public $database;
		private $session;
		private $post;
		private $get;
		private $db;
		
		public function __construct() {
			$db = new Database();
			$this -> db = $db;
			$this -> post = $_POST;
			$this -> get = $_GET;
		}
	
		public function getClass() {
			$rows = array(
				'classid',
				'classname',
				'classcode'
			);
		
			$data = $this -> db -> select('class', $rows);
			
			return $data; 
		}
		
		public function getComments() {
			$classid = !empty($this -> post['classid']) ? $this -> post['classid']: 1;
			
			$rows = array(
				'commentid',
				'classid',
				'header',
				'text',
				'vote'
			);
			
			$data = $this -> db -> selectWhere('comment', $rows, 'classid', $classid ); 
			
			return $data;
		}
		
		public function insertComment() {
			$classid = $this -> post['classid'];
			$header = $this -> post['header'];
			$text = $this -> post['text'];
			
			$rows = array(
				$classid,
				$header,
				$text
			);
			
			$data = $this -> db -> insertComment($rows);
			
			return $data;
		}
	
	}

?>