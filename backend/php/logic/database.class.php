<?php
	// This class conducts database operations. 
	// Variable $db is a global variable referring to the instantiation of a PDO object in db.php
	class Database {
		public $db;
		public $config;
		
		public function __construct() {
			global $db;
			global $config;
			$this -> db = $db;
		}
		
		// Checks if something exists in the database.
		public function compare ($table, $row, $data) {
			if (is_array($row)) {
				$row = implode(', ', $row);
			}
			$sth = $this -> db -> prepare("SELECT $row FROM $table WHERE $row = :data");
			$sth -> bindParam(':data', $data);
			$sth -> execute();
			if ($sth -> rowCount() == 0) {
				$sth -> closeCursor();
				return true;
			}
			$sth -> closeCursor();
			return false;
		}
		
		// Generic function to select rows where a certain column has a value  
		public function selectWhere ($table, $row, $where, $data) {
			if (is_array($row)) {
				$row = implode(', ', $row);
			}
			$sth = $this -> db -> prepare("SELECT $row FROM $table WHERE $where = :data");
			$sth -> bindParam(':data', $data);
			$sth -> execute();
			$data = $sth -> fetchAll(PDO::FETCH_ASSOC);
			if ($sth -> rowCount() == 0) {
				$sth -> closeCursor();
				if ($this -> config['debug'] == true) {
					return $sth -> errorInfo();
				}
				return false;
			}
			$sth -> closeCursor();
			return $data;
		}
		
		// Generic function to select and join columns  
		public function innerJoin ($table, $table2, $row, $on) {
			if (is_array($row)) {
				$row = implode(', ', $row);
			}
			$sth = $this -> db -> prepare("
				SELECT $row
				FROM $table
				INNER JOIN $table2
				ON $table.$on=$table2.$on;
				
			");
			$sth -> bindParam(':data', $data);
			$sth -> execute();
			$data = $sth -> fetch(PDO::FETCH_ASSOC);
			if ($sth -> rowCount() == 0) {
				$sth -> closeCursor();
				if ($this -> config['debug'] == true) {
					return $sth -> errorInfo();
				}
				return false;
			}
			$sth -> closeCursor();
			return $data;
		}
		
		// Generic function to select rows 
		public function select ($table, $row) {
			if (is_array($row)) {
				$row = implode(', ', $row);
			}
			$sth = $this -> db -> prepare("SELECT $row FROM $table");
			$sth -> execute();
			$data = $sth -> fetchAll(PDO::FETCH_ASSOC);
			if ($sth -> rowCount() == 0) {
				$sth -> closeCursor();
				if ($this -> config['debug'] == true) {
					return $sth -> errorInfo();
				}
				return false;
			}
			$sth -> closeCursor();
			return $data;
		}
		
		// Generic update function
		public function update ($table, $row, $where, $data) {
			if (is_array($data)) {
				return "Error: function database -> update does not accept arrays";
			}
			$this -> db -> beginTransaction();
			$this -> db -> query("LOCK TABLE user WRITE");
			$sth = $this -> db -> prepare("
				UPDATE user 
				SET $row = :data
				WHERE userid = :userid
			");
			$th -> bindParam(':data', $data);
			$th -> bindParam(':userid', $where);
			$sth -> execute();
			if ($sth -> rowCount() == 0) {
				$this -> db -> query("UNLOCK TABLES");
				$this -> db -> rollBack();
				$sth -> closeCursor();
				return "Error: Database row not updated!";
			}
			$this -> db -> query("UNLOCK TABLES");
			$this -> db -> commit();
			$sth -> closeCursor();
			return true;
		}
		
		// Generic function to delete rows where a certain column has a value  
		public function delete ($table, $row, $data) {
			if (is_array($row)) {
				$row = implode(', ', $row);
			}
			$this -> db -> beginTransaction();
			$this -> db -> query("LOCK TABLE $table WRITE");
			$sth = $this -> db -> prepare("DELETE FROM $table WHERE $row = :data");
			$sth -> bindParam(':data', $data);
			$sth -> execute();
			if ($sth -> rowCount() == 0) {
				$this -> db -> query("UNLOCK TABLES");
				$this -> db ->rollBack();
				$sth -> closeCursor();
				throw New exception("Error: Database row not deleted!");
			}
			$this -> db -> query("UNLOCK TABLES");
			$this -> db -> commit();
			$sth -> closeCursor();
			return true;
		}

		
		// Function to insert data related to a site accepts parameter $data an array
		public function insertComment ($data) {
			if (!is_array($data)) {
				return "Error: function database -> insertSite only accepts arrays";
			}
			$this -> db -> beginTransaction();
			$this -> db -> query("LOCK TABLE site WRITE");
			$sth = $this -> db -> prepare("INSERT INTO comment (classid, header, text) VALUES(?, ?, ?)");
			$sth -> execute($data);
			if ($sth -> rowCount() == 0) {
				$this -> db -> query("UNLOCK TABLES");
				$this -> db -> rollBack();
				$sth -> closeCursor();
				return "Error: Site not inserted";
			}
			$this -> db -> commit();
			$sth -> closeCursor();
			return true;
		}
		
	}	
?>